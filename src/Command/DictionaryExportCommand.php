<?php

namespace App\Command;

use App\Entity\Dictionary;
use App\Entity\Entry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DictionaryExportCommand extends Command
{
	protected static $defaultName = 'app:dictionary:export';

	private $em;

	public function __construct(string $name = null, EntityManagerInterface $entityManager)
	{
		parent::__construct($name);

		$this->em = $entityManager;
	}

	protected function configure()
	{
		$this
			->setDescription('Add a short description for your command')
			->addArgument('dictionary_name', InputArgument::REQUIRED, 'Dictionary name')
			->addArgument('file', InputArgument::REQUIRED, 'File with words');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$io = new SymfonyStyle($input, $output);

		$dictionaryName = $input->getArgument('dictionary_name');
		$filePath       = $input->getArgument('file');

		if (!file_exists($filePath)) {
			$io->error(sprintf("File '%s' not found!", $filePath));
			return;
		}

		$dictionary = new Dictionary();
		$dictionary->setTitle($dictionaryName);

		$this->em->persist($dictionary);
		$this->em->flush($dictionary);
		if (empty($dictionary->getId())) {
			$io->error("Dictionary not saved!");
			return;
		}

		$content = file_get_contents($filePath);
		$data    = json_decode($content, true);
		$words   = $data['data'] ?? [];

		$progress = new ProgressBar($output, count($words));

		$persisted = 0;
		foreach ($words as $word) {
			$entry = new Entry();
			$entry->setWord($word['word']);
			$entry->setDictionaryId($dictionary->getId());
			$entry->setTranscription($word['transcription'] ?? null);
			$entry->setTranslation($word['translation']);
			$this->em->persist($entry);
			$persisted++;

			if ($persisted % 50) {
				$this->em->flush();
			}

			$progress->advance();
		}

		$progress->finish();

		$io->success('You have a new command! Now make it your own! Pass --help to see your options.');
	}
}
