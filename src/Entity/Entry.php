<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EntryRepository")
 */
class Entry
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $word;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $translation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $transcription;

	/**
	 * @var int|null
	 *
	 * @ORM\Column(type="integer")
	 */
    private $dictionaryId;

	/**
	 * @var int|null
	 *
	 * @ORM\Column(type="integer", name="correct_answered")
	 */
    private $correctAnswered = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWord(): ?string
    {
        return $this->word;
    }

    public function setWord(string $word): void
    {
        $this->word = $word;
    }

    public function getTranslation(): ?string
    {
        return $this->translation;
    }

    public function setTranslation(string $translation): void
    {
        $this->translation = $translation;
    }

    public function getTranscription(): ?string
    {
        return $this->transcription;
    }

    public function setTranscription(?string $transcription): void
    {
        $this->transcription = $transcription;
    }

	/**
	 * @return int|null
	 */
	public function getDictionaryId(): ?int
	{
		return $this->dictionaryId;
	}

	/**
	 * @param int|null $dictionaryId
	 */
	public function setDictionaryId(?int $dictionaryId): void
	{
		$this->dictionaryId = $dictionaryId;
	}

	/**
	 * @return int|null
	 */
	public function getCorrectAnswered(): ?int
	{
		return $this->correctAnswered;
	}

	/**
	 * @param int|null $correctAnswered
	 */
	public function setCorrectAnswered(int $correctAnswered): void
	{
		$this->correctAnswered = max(0, $correctAnswered);
	}

	public function increaseCorrectAnswered(int $times = 1): void
	{
		$this->setCorrectAnswered($this->getCorrectAnswered() + $times);
	}

	public function decreaseCorrectAnswered(int $times = 1): void
	{
		$this->setCorrectAnswered($this->getCorrectAnswered() - $times);
	}
}
