<?php

namespace App\Controller;

use App\Entity\Entry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EntryController extends AbstractController
{
	/**
	 * @Route("/entry", name="entry")
	 */
	public function index()
	{
		return $this->render('entry/index.html.twig', [
			'controller_name' => 'EntryController',
		]);
	}

	/**
	 * @Route("/{dictionaryId}/entry/new", name="entry.new", requirements={"dictionaryId"="\d+"}, methods={"POST"})
	 */
	public function new(int $dictionaryId, Request $request)
	{
		$entry = new Entry();
		$form  = $this->createFormBuilder($entry)
			->add('word', TextType::class)
			->add('translation', TextType::class)
			->add('transcription', TextType::class)
			->add('save', SubmitType::class, ['label' => 'Add'])
			->getForm();

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$entry = $form->getData();
			$entry->setDictionaryId($dictionaryId);

			$em = $this->getDoctrine()->getManager();
			$em->persist($entry);
			$em->flush();
		}

		return $this->redirectToRoute('dictionary.details', ['dictionaryId' => $dictionaryId]);
	}

	/**
	 * @param int $entryId
	 * @Route("/{dictionaryId}/{entryId}/delete", name="entry.delete", requirements={"entryId"="\d+", "dictionaryId"="\d+"})
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function delete(int $dictionaryId, int $entryId)
	{
		$em    = $this->getDoctrine()->getManager();
		$entry = $em->getRepository(Entry::class)->find($entryId);
		if (empty($entry)) {
			throw $this->createNotFoundException("Entry not found");
		}

		$em->remove($entry);
		$em->flush();

		return $this->redirectToRoute('dictionary.details', ['dictionaryId' => $dictionaryId]);
	}
}
