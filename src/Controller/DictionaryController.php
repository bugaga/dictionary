<?php

namespace App\Controller;

use App\Entity\Dictionary;
use App\Entity\Entry;
use App\Model\Constant\NotificationLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DictionaryController extends AbstractController
{
	/**
	 * @Route("/")
	 * @Route("/dictionary", name="dictionary.list")
	 */
	public function index()
	{
		$dictionaries = $this->getDoctrine()->getRepository(Dictionary::class)->findAll();
		$entriesCount = $this->getDoctrine()->getRepository(Dictionary::class)->getEntriesCountToEveryDictionary();

		return $this->render(
			'dictionary/index.html.twig', [
				'dictionaries' => $dictionaries,
				'entriesCount' => $entriesCount,
			]
		);
	}

	/**
	 * @Route("/dictionary/new", name="dictionary.new")
	 *
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function new(Request $request)
	{
		$dictionary = new Dictionary();

		$form = $this->createFormBuilder($dictionary)
			->add('title', TextType::class)
			->add('save', SubmitType::class, ['label' => 'Create Dictionary'])
			->getForm();

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$dictionary = $form->getData();

			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($dictionary);
			$entityManager->flush();

			$this->addFlash(NotificationLevel::SUCCESS, sprintf('Dictionary "%s" added!', $dictionary->getTitle()));

			return $this->redirectToRoute('dictionary.list');
		}

		return $this->render(
			'dictionary/new.html.twig',
			[
				'form' => $form->createView()
			]
		);
	}

	/**
	 * @Route("/dictionary/{dictionaryId}", name="dictionary.update", requirements={"dictionaryId"="\d+"}, methods={"POST"})
	 *
	 * @param Request $request
	 * @param int $dictionaryId
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function update(int $dictionaryId, Request $request)
	{
		$entityManager = $this->getDoctrine()->getManager();

		$dictionary = $entityManager->getRepository(Dictionary::class)->find($dictionaryId);
		if (empty($dictionary)) {
			throw $this->createNotFoundException('Dictionary not found');
		}

		$form = $this->createFormBuilder($dictionary)
			->add('title', TextType::class)
			->add('save', SubmitType::class, ['label' => 'Save'])
			->getForm();

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$dictionary = $form->getData();
			$entityManager->persist($dictionary);
			$entityManager->flush();
		}

		return $this->redirectToRoute('dictionary.details', ['dictionaryId' => $dictionaryId]);
	}

	/**
	 * @Route("/dictionary/{dictionaryId}", name="dictionary.details", requirements={"dictionaryId"="\d+"}, methods={"GET"})
	 * @param int $dictionaryId
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function details(int $dictionaryId)
	{
		$entityManager = $this->getDoctrine()->getManager();

		$dictionary = $entityManager->getRepository(Dictionary::class)->find($dictionaryId);
		if (empty($dictionary)) {
			throw $this->createNotFoundException('Dictionary not found');
		}

		$dictionaryForm = $this->createFormBuilder(
			$dictionary,
			[
				'action' => $this->generateUrl('dictionary.update', ['dictionaryId' => $dictionaryId])
			]
		)
			->add('title', TextType::class)
			->add('save', SubmitType::class, ['label' => 'Save'])
			->getForm();

		$entry = new Entry();
		$entry->setDictionaryId($dictionaryId);
		$entryForm = $this->createFormBuilder($entry, ['action' => $this->generateUrl('entry.new', ['dictionaryId' => $dictionaryId])])
			->add('word', TextType::class)
			->add('translation', TextType::class)
			->add('transcription', TextType::class, ['required' => false])
			->add('save', SubmitType::class, ['label' => 'Add'])
			->getForm();

		$entries = $entityManager->getRepository(Entry::class)->findByDictionary($dictionary);

		return $this->render(
			'dictionary/details.html.twig',
			[
				'dictionary_form' => $dictionaryForm->createView(),
				'entry_form'      => $entryForm->createView(),
				'dictionary'      => $dictionary,
				'entries'         => $entries
			]
		);
	}

	/**
	 * @Route("/dictionary/{dictionaryId}/delete", name="dictionary.delete", requirements={"dictionaryId"="\d+"})
	 *
	 * @param int $dictionaryId
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function delete(int $dictionaryId)
	{
		$entityManager = $this->getDoctrine()->getManager();
		$dictionary    = $entityManager->getRepository(Dictionary::class)->find($dictionaryId);
		if (empty($dictionary)) {
			throw $this->createNotFoundException("Dictionary not found");
		}

		$entityManager->getRepository(Entry::class)->deleteByDictionaryId($dictionary);
		$entityManager->remove($dictionary);
		$entityManager->flush();

		return $this->redirectToRoute('dictionary.list');
	}
}
