<?php

namespace App\Controller;

use App\Entity\Dictionary;
use App\Entity\Entry;
use App\Model\Constant\NotificationLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class LearningProcessController extends AbstractController
{
	public const  CORRECT_ANSWERS_TO_REMEMBER = 3;
	private const CORRECT_ANSWER_CONFIG       = 'correct_answer';

	/**
	 * @Route("/learning/{dictionaryId}", name="learning_process", requirements={"dictionaryId"="\d+"})
	 * @param int $dictionaryId
	 * @param SessionInterface $session
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function learn(int $dictionaryId, SessionInterface $session, Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		$dictionary = $em->getRepository(Dictionary::class)->find($dictionaryId);
		if (empty($dictionary)) {
			throw $this->createNotFoundException("Dictionary not found");
		}

		if ($request->query->has('answer') && $session->has(self::CORRECT_ANSWER_CONFIG)) {
			$userAnswer    = $request->query->getInt('answer');
			$correctAnswer = (int)$session->get(self::CORRECT_ANSWER_CONFIG);
			$session->remove(self::CORRECT_ANSWER_CONFIG);

			$entry = $em->getRepository(Entry::class)->find($correctAnswer);
			if (empty($entry)) {
				throw $this->createNotFoundException(sprintf('Cannot find correct entry "%s" from session!', $correctAnswer));
			}

			if ($userAnswer === $correctAnswer) {
				$entry->increaseCorrectAnswered();
				$this->addFlash(
					'info',
					'CORRECT!'
				);
			} else {
				$entry->decreaseCorrectAnswered();
				$this->addFlash(
					'error',
					sprintf('You are wrong. Correct answer is "%s".', $entry->getTranslation())
				);
			}

			$em->flush();
		}

		$allEntries = $em->getRepository(Entry::class)->findByDictionary($dictionary);
		if (count($allEntries) < 4) {
			$this->addFlash(
				'warning',
				'Add more words to learn!'
			);
			return $this->redirectToRoute('dictionary.details', ['dictionaryId' => $dictionaryId]);
		}

		$correctAnswersToRemember = self::CORRECT_ANSWERS_TO_REMEMBER;
		$notLearned               = array_filter(
			$allEntries,
			function (Entry $entry) use ($correctAnswersToRemember) {
				return $entry->getCorrectAnswered() < $correctAnswersToRemember;
			}
		);

		if (empty($notLearned)) {
			throw new \LogicException("You have learned all entries!");
		}

		$variantsToLearn = array_slice($notLearned, 0, 10);
		$variants[]      = $correctAnswer = $variantsToLearn[mt_rand(0, count($variantsToLearn) - 1)];

		shuffle($allEntries);
		foreach ($allEntries as $entry) {
			if ($entry->getId() === $correctAnswer->getId()) {
				continue;
			}

			if (count($variants) >= 4) {
				break;
			}

			$variants[] = $entry;
		}

		shuffle($variants);

		$session->set(self::CORRECT_ANSWER_CONFIG, $correctAnswer->getId());

		$viewVariables['dictionaryId']   = $dictionaryId;
		$viewVariables['variants']       = $variants;
		$viewVariables['conceivedWorld'] = $correctAnswer->getWord();

		return $this->render('learning_process/index.html.twig', $viewVariables);
	}

	/**
	 * @Route("/learning/{dictionaryId}/reset-progress", name="learning_process.reset_progress", requirements={"dictionaryId"="\d+"})
	 * @param int $dictionaryId
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function resetProgress(int $dictionaryId)
	{
		$em = $this->getDoctrine()->getManager();

		$dictionary = $em->getRepository(Dictionary::class)->find($dictionaryId);
		if (empty($dictionary)) {
			throw $this->createNotFoundException("Dictionary not found");
		}

		$em->getRepository(Entry::class)->resetProgress($dictionary);

		$this->addFlash(NotificationLevel::PRIMARY, "Start from begin!");

		return $this->redirectToRoute('dictionary.details', ['dictionaryId' => $dictionaryId]);
	}
}
