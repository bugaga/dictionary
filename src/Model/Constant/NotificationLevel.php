<?php

namespace App\Model\Constant;

final class NotificationLevel
{
	public const PRIMARY = 'primary';
	public const SUCCESS = 'success';
	public const WARNING = 'warning';
	public const DANGER  = 'danger';
}