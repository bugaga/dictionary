<?php

namespace App\Repository;

use App\Entity\Dictionary;
use App\Entity\Entry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Dictionary|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dictionary|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dictionary[]    findAll()
 * @method Dictionary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DictionaryRepository extends ServiceEntityRepository
{
	public function __construct(RegistryInterface $registry)
	{
		parent::__construct($registry, Dictionary::class);
	}

	public function getEntriesCountToEveryDictionary()
	{
		$response = $this->createQueryBuilder('d')
			->select('d.id, count(e.id) as entriesCount')
			->leftJoin(Entry::class, 'e', Join::WITH, 'e.dictionaryId = d.id')
			->groupBy('d.id')
			->getQuery()
			->getResult(AbstractQuery::HYDRATE_ARRAY);

		return array_combine(
			array_column($response, 'id'),
			array_column($response, 'entriesCount')
		);
	}
}
