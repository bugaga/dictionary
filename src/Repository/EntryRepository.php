<?php

namespace App\Repository;

use App\Entity\Dictionary;
use App\Entity\Entry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Entry|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entry|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entry[]    findAll()
 * @method Entry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EntryRepository extends ServiceEntityRepository
{
	public function __construct(RegistryInterface $registry)
	{
		parent::__construct($registry, Entry::class);
	}

	/**
	 * @param Dictionary $dictionary
	 * @return Entry[]
	 */
	public function findByDictionary(Dictionary $dictionary): array
	{
		return $this->createQueryBuilder('e')
			->andWhere('e.dictionaryId = :dictionaryId')
			->setParameter('dictionaryId', $dictionary->getId())
			->orderBy('e.word', 'ASC')
			->getQuery()
			->getResult();
	}

	/**
	 * @param Dictionary $dictionary
	 * @param int $enoughAnswered
	 * @return Entry[]
	 */
	public function findToLearnByDictionary(Dictionary $dictionary, int $enoughAnswered): array
	{
		return $this->createQueryBuilder('e')
			->andWhere('e.dictionaryId = :dictionaryId')
			->andWhere('e.correctAnswered < :enough_answered')
			->setParameter('dictionaryId', $dictionary->getId())
			->setParameter('enough_answered', $enoughAnswered)
			->orderBy('e.word', 'ASC')
			->getQuery()
			->getResult();
	}

	public function deleteByDictionaryId(Dictionary $dictionary): void
	{
		$this->createQueryBuilder('e')
			->andWhere('e.dictionaryId = :dictionaryId')
			->setParameter('dictionaryId', $dictionary->getId())
			->delete()
			->getQuery()
			->execute();
	}

	public function resetProgress(Dictionary $dictionary): void
	{
		$this->createQueryBuilder('e')
			->update()
			->set('e.correctAnswered', 0)
			->where('e.dictionaryId = :dictionaryId')
			->setParameter('dictionaryId', $dictionary->getId())
			->getQuery()
			->execute();
	}
}
